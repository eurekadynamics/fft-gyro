clear all
clc

%FFT Gyro Matlab Script Sample Position March 2021

% * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
%                                           Parametros
% * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

COMM_PORT = 'COM3';
SystemMode = 'Motor';
data_rate = 10; %Rango 1:1000 solo enteros.
Samples = 30000;

ModeM1 = 2; %1 = Wheel  2=Joint;   0:No change
ModeM2 = 2; %1 = Wheel  2 = Joint 0:No change
ModeM3 = 2; %1 = Wheel  2 = Joint  0:No change

LED_M1 = 1;
LED_M2 = 1;
LED_M3 = 1;

%Torque
TorqueLimM1 = 600;
setTL1      = 0;
MaxTorqueM1 = 600;
setMT1      = 0;
setTorqueEnableM1 = 1;

TorqueLimM2 = 600;
setTL2      = 0;
MaxTorqueM2 = 600;
setMT2      = 0;
setTorqueEnableM2 = 1;

TorqueLimM3 = 600;
setTL3      = 0;
MaxTorqueM3 = 600;
setMT3      = 0;
setTorqueEnableM3 = 1;

Testpos = 1;

%Position
PosM1=Testpos;
setPosM1=1;
MovVel1=500;
setVelM1=1;
CW_AngleLimitM1=1;
CCW_AngleLimitM1=1023;
setAngleLimitM1=3;%Set angle limits  1:CW  2:CCW   3:Both CW and CCW

PosM2=Testpos;
setPosM2=1;
MovVel2=500;
setVelM2=1;
CW_AngleLimitM2=1;
CCW_AngleLimitM2=1023;
setAngleLimitM2=3;%Set angle limits  1:CW  2:CCW   3:Both CW and CCW

PosM3=Testpos;
setPosM3=1;
MovVel3=500;
setVelM3=1;
CW_AngleLimitM3=1;
CCW_AngleLimitM3=1023;
setAngleLimitM3=3;%Set angle limits  1:CW  2:CCW   3:Both CW and CCW

% * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
%                                           Inicialización
% * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 


Y =  zeros(1,Samples, 'uint8');
X =  zeros(1,Samples);

ZeroRelativo = [340.13,51.59,34.36];

% * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
%                                           1) Abro el puerto Serial
% * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

delete(instrfind({'Port'},{COMM_PORT}));
puerto_serial = serial(COMM_PORT,'baudrate',9600,'databits',8, 'parity','none','stopbits',1,'readasyncmode','continuous');
warning('off','MATLAB:serial:fscanf:unsuccessfulRead');
%Abro el puerto Serial
fopen(puerto_serial);

% * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
%                                       Ciclo principal de lectura y
%                                           escritura
% * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

% 1.- Configuración e inicialización del data rate.
setMotorsConfiguration(puerto_serial,ModeM1,ModeM2,ModeM3, LED_M1,LED_M2,LED_M3);
setDataRate(puerto_serial,data_rate);


% 2.- Entro al ciclo de captura de datos.
contador_muestras=1;
numero_muestras = Samples;
Flag=false;

%Debug
pos_aux=0;
flagCW=true;
%debug

%while (1) 
     pause(0.01);
     clc
     if strcmp(SystemMode, 'Motor')
         
         if (Flag==false)
            
            %Torque 
            sendPacket1ToGyroboard(puerto_serial,TorqueLimM1,setTL1,MaxTorqueM1,setMT1,setTorqueEnableM1, TorqueLimM2,setTL2,MaxTorqueM2,setMT2,setTorqueEnableM2, TorqueLimM3,setTL3,MaxTorqueM3,setMT3,setTorqueEnableM3);          %0-1023  CCW  y 1024-2047 CW
            %Debug
            if(flagCW)
                if pos_aux>=1023
                    flagCW=false;
                else
                    pos_aux=pos_aux+3;    
                end
                
            else
                
                if(pos_aux<=0)
                    flagCW=true;
                else
                    pos_aux=pos_aux-3;    
                end
                
            end
            %Debug
            
            %PosM1= pos_aux;

            %Position 
            sendPacket2ToGyroboard(puerto_serial,PosM1,setPosM1,MovVel1,setVelM1,CW_AngleLimitM1,CCW_AngleLimitM1,setAngleLimitM1,PosM2,setPosM2,MovVel2,setVelM2,CW_AngleLimitM2,CCW_AngleLimitM2,setAngleLimitM2,PosM3,setPosM3,MovVel3,setVelM3,CW_AngleLimitM3,CCW_AngleLimitM3,setAngleLimitM3);
            
            %pause(0.05);
            
            Flag=true;
         end    
         
     end
     
    
    %Gyrobaord Read
    pause(5);
    [Data1, Data2, Data3] = getDataFromGyroboard(puerto_serial,32, SystemMode);
    %pause(0.05);

    contador_muestras = contador_muestras+1;
     
%end

fclose(puerto_serial);
