function  [Data1,Data2,Data3] = getDataFromGyroboard(puerto_serial,Nbytes,SystemMode)
%INPUT
%puerto_serial es el puerto por el que se establecio la comunicaci�n con el gyroboard.
%Nbytes es una variable constante que indica el numero de bytes a leer del puerto serial(en esta versi�n siempre ser� de 32 bytes)
%SystemMode es el modo en el que el sistema se encuentra, es un string y
%puede ser 'Motor' o 'Encoder'.

%OUTPUT
%Modo Motor
%Data1,Data2,Data3 son matrices que tienen guardados 6 valores cada uno que corresponden a la siguiente informacion(1,2 y 3 corresponde al n�mero de motor): 
%     ErrorMotor;
%     TorqueMotor;
%     VelocityMotor;
%     PositionMotor;
%     VoltageMotor;
%     TemperatureMotor;

%Modo Encoder
%Data1,Data2 y Data3 corresponden a los valores de los encoders 1,2, y 3 respectivamente

clear Out_aux

if strcmp(SystemMode,'Motor')
    uint8 Out_aux;
    Out_aux =  fread(puerto_serial,Nbytes, 'uint8');
    % * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
    %                                                                       Motor 1
    % * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
    
    if(size(Out_aux,1)<32)
        nop=1;
    end
    
    % Error M1: byte 4
    ErrorM1 = Out_aux(4,:);
    %Torque M1: byte 7 y 8  
    TorqueM1=typecast([uint8(Out_aux(7,:)) uint8(Out_aux(8,:))],'uint16');
    %Velocidad M1: byte 13 y 14
    VelocityM1=typecast([uint8(Out_aux(13,:)) uint8(Out_aux(14,:))],'uint16');
    %Posicion M1: byte 19 y 20
    PositionM1=typecast([uint8(Out_aux(19,:)) uint8(Out_aux(20,:))],'uint16');
    %Voltaje M1: byte 25
    VoltageM1=uint8(Out_aux(25,:));
    %Temperatura M1: byte 28
    TemperatureM1=uint8(Out_aux(28,:));
    
    
    Motor1(1,:) = ErrorM1;
    Motor1(2,:) = double(bitand(TorqueM1,uint16(1023)))*0.1; %TorqueM1; 
    Motor1(3,:) = double(bitand(VelocityM1,uint16(1023)))*0.1; %VelocityM1;
    Motor1(4,:) = double(PositionM1)*0.29;
    Motor1(5,:) = double(VoltageM1)/10;
    Motor1(6,:) = TemperatureM1;
    
    fprintf([' * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   \n']);
    fprintf(['                                                            Motor 1                                                                        \n']);
    fprintf([' * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   \n']);    
    Motor1_aux = Motor1;
    Motor1_aux(2,:) = TorqueM1;
    Motor1_aux(3,:) = VelocityM1;
    DisplayInformation(Motor1_aux,SystemMode);
    
    
    % * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    %                                                                      Motor 2
    % * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    
    
    % Error M2: byte 5
    ErrorM2 = Out_aux(5,:);
    out2 = errorMotor(ErrorM2);
    %Torque M2: byte 9 y 10
    TorqueM2=typecast([uint8(Out_aux(9,:)) uint8(Out_aux(10,:))],'uint16');
    %Velocidad M2: byte 15 y 16
    VelocityM2=typecast([uint8(Out_aux(15,:)) uint8(Out_aux(16,:))],'uint16');
    %Posicion M2: byte 21 y 22
    PositionM2 = typecast([uint8(Out_aux(21,:)) uint8(Out_aux(22,:))],'uint16');
    %Voltaje M2: byte 26
    VoltageM2=uint8(Out_aux(26,:));
    %Temperatura M2: byte 29
    TemperatureM2=uint8(Out_aux(29,:));
    
    
    Motor2(1,:) = ErrorM2;
    Motor2(2,:) = double(bitand(TorqueM2,uint16(1023)))*0.1; %TorqueM2;
    Motor2(3,:) = double(bitand(VelocityM2,uint16(1023)))*0.1; %VelocityM2;
    Motor2(4,:) = double(PositionM2)*0.29; 
    Motor2(5,:) = double(VoltageM2)/10;
    Motor2(6,:) = TemperatureM2;

    fprintf([' * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   \n']);
    fprintf(['                                                            Motor 2                                                                        \n']);
    fprintf([' * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   \n']);
    Motor2_aux = Motor2;
    Motor2_aux(2,:) = TorqueM2;
    Motor2_aux(3,:) = VelocityM2;
    DisplayInformation(Motor2_aux,SystemMode);
    
    % * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    %                                                                       Motor 3                                                       
    % * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    
    % Error M3: byte 6
    ErrorM3 = Out_aux(6,:);
    out3 = errorMotor(ErrorM3);
    %Torque M3: byte 11 y 12
    TorqueM3=typecast([uint8(Out_aux(11,:)) uint8(Out_aux(12,:))],'uint16');
    %Velocidad M3: byte 17 y 18
    VelocityM3=typecast([uint8(Out_aux(17,:)) uint8(Out_aux(18,:))],'uint16');
    %Posicion M3: byte 23 y 24
    PositionM3 = typecast([uint8(Out_aux(23,:)) uint8(Out_aux(24,:))],'uint16');
    %Voltaje M3: byte 27
    VoltageM3=uint8(Out_aux(27,:));
    %Temperatura M3: byte 30
    TemperatureM3=uint8(Out_aux(30,:));

    Motor3(1,:) = ErrorM3;
    Motor3(2,:) =  double(bitand(TorqueM3,uint16(1023)))*0.1; %TorqueM3;
    Motor3(3,:) = double(bitand(VelocityM3,uint16(1023)))*0.1; %VelocityM3;
    Motor3(4,:) = double(PositionM3)*0.29; 
    Motor3(5,:) = double(VoltageM3)/10;
    Motor3(6,:) = TemperatureM3;
    
    fprintf([' * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   \n']);
    fprintf(['                                                            Motor 3                                                                        \n']);
    fprintf([' * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   \n']);
    Motor3_aux = Motor3;
    Motor3_aux(2,:) = TorqueM3;
    Motor3_aux(3,:) = VelocityM3;
    DisplayInformation(Motor3_aux,SystemMode);
    

    fprintf([' * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   \n']);
    fprintf([' * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   \n']);

    
    Data1 = Motor1;
    Data2 = Motor2;
    Data3 = Motor3;
elseif strcmp(SystemMode,'Encoder')
%     clc
    char Out_aux;
    Out_aux =  fread(puerto_serial,Nbytes, 'char');
%     pause(0.01)
%     size(Out_aux)


    % * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
    %                                                                       Encoder 1
    % * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
    %PositionM1: byte 4 al 11
    PositionE1_aux = char(Out_aux(5:12,:));
    PositionE1 = str2double(PositionE1_aux');  %str2double( char(u(5:12,:))');
        
%     fprintf([' * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   \n']);
%     fprintf(['                                                            Encoder 1                                                                        \n']);
%     fprintf([' * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   \n']);
%     DisplayInformation(PositionE1,SystemMode);
    
%     % * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
%     %                                                                       Encoder 2
%     % * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
%     %PositionM1: byte 14 al 21
        PositionE2_aux = char(Out_aux(14:21,:));
        PositionE2 = str2double(PositionE2_aux');
     
%     fprintf([' * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   \n']);
%     fprintf(['                                                            Encoder 2                                                                        \n']);
%     fprintf([' * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   \n']);
%     DisplayInformation(PositionE2,SystemMode);
    
%     % * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
%     %                                                                       Encoder 3
%     % * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
    %PositionM1: byte 23 al 30
    PositionE3_aux = char(Out_aux(23:30,:));
    PositionE3 = str2double(PositionE3_aux');
    
%     fprintf([' * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   \n']);
%     fprintf(['                                                            Encoder 3                                                                        \n']);
%     fprintf([' * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   \n']);
%     DisplayInformation(PositionE3,SystemMode);
    
    
%     fprintf([' * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   \n']);
%     fprintf([' * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   \n']);
    
    Data1 = PositionE1;
    Data2 = PositionE2;
    Data3 = PositionE3;
    nop=1;
end

Out=Out_aux;
end