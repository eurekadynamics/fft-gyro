clear all
clc

%FFT Gyro Matlab Script Sample Position March 2021

% * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
%                                           Parametros
% * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

COMM_PORT = 'COM5';
SystemMode = 'Motor';
data_rate = 1; %Rango 1:1000 solo enteros.

ModeM1 = 1; %1 = Wheel  2 = Joint;   0:No change
ModeM2 = 1; %1 = Wheel  2 = Joint 0:No change
ModeM3 = 1; %1 = Wheel  2 = Joint  0:No change

LED_M1 = 0;
LED_M2 = 0;
LED_M3 = 0;

%Position
PosM1       = 0;
setPosM1    = 0;
MovVel1     = 100;
setVelM1    = 1;
CW_AngleLimitM1     = 0;
CCW_AngleLimitM1    = 0;
setAngleLimitM1     = 3;%Set angle limits  1:CW  2:CCW   3:Both CW and CCW

PosM2       = 0;
setPosM2    = 0;
MovVel2     = 100;
setVelM2    = 1;
CW_AngleLimitM2     = 0;
CCW_AngleLimitM2    = 0;
setAngleLimitM2     = 3;%Set angle limits  1:CW  2:CCW   3:Both CW and CCW

PosM3       = 0;
setPosM3    = 0;
MovVel3     = 100;
setVelM3    = 1;
CW_AngleLimitM3     = 0;
CCW_AngleLimitM3    = 0;
setAngleLimitM3     = 3;%Set angle limits  1:CW  2:CCW   3:Both CW and CCW


% * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
%                                           1) Abro el puerto Serial
% * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

delete(instrfind({'Port'},{COMM_PORT}));
puerto_serial = serial(COMM_PORT,'baudrate',9600,'databits',8, 'parity','none','stopbits',1,'readasyncmode','continuous');
warning('off','MATLAB:serial:fscanf:unsuccessfulRead');
fopen(puerto_serial);

% * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
%                                       Ciclo principal de lectura y
%                                           escritura
% * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

% 1.- Configuración e inicialización del data rate.
setMotorsConfiguration(puerto_serial,ModeM1,ModeM2,ModeM3, LED_M1,LED_M2,LED_M3);
pause(0.01);
setDataRate(puerto_serial,data_rate);


% 2.- Entro al ciclo de captura de datos.
contador_muestras = 1;
Flag              = false;

while (1) %contador_muestras <= numero_muestras
     pause(0.01);
     clc
     if strcmp(SystemMode, 'Motor')
         
         if (Flag==false)
            
            %Velocity
            sendPacket2ToGyroboard(puerto_serial,PosM1,setPosM1,MovVel1,setVelM1,CW_AngleLimitM1,CCW_AngleLimitM1,setAngleLimitM1,PosM2,setPosM2,MovVel2,setVelM2,CW_AngleLimitM2,CCW_AngleLimitM2,setAngleLimitM2,PosM3,setPosM3,MovVel3,setVelM3,CW_AngleLimitM3,CCW_AngleLimitM3,setAngleLimitM3);
            
            Flag=true;
         end    
     end
    
    %Gyrobaord Read
    [Data1, Data2, Data3] = getDataFromGyroboard(puerto_serial,32, SystemMode);

    contador_muestras = contador_muestras+1;
     
end

fclose(puerto_serial);