clear all
clc

%FFT Gyro Matlab Script Sample Position March 2021

% * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
%                                           Parametros
% * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

COMM_PORT = 'COM8';
SystemMode = 'Encoder';
data_rate = 1; %Rango 1:1000 solo enteros.

ModeM1 = 0; %1 = Wheel  2 = Joint;   0:No change
ModeM2 = 0; %1 = Wheel  2 = Joint 0:No change
ModeM3 = 0; %1 = Wheel  2 = Joint  0:No change

LED_M1 = 1;
LED_M2 = 1;
LED_M3 = 1;


% * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
%                                           1) Abro el puerto Serial
% * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

delete(instrfind({'Port'},{COMM_PORT}));
puerto_serial = serial(COMM_PORT,'baudrate',9600,'databits',8, 'parity','none','stopbits',1,'readasyncmode','continuous');
warning('off','MATLAB:serial:fscanf:unsuccessfulRead');
fopen(puerto_serial);

% * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
%                                       Ciclo principal de lectura y
%                                           escritura
% * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

% 1.- Configuración e inicialización del data rate.
setMotorsConfiguration(puerto_serial,ModeM1,ModeM2,ModeM3, LED_M1,LED_M2,LED_M3);
pause(0.01);
setDataRate(puerto_serial,data_rate);


t=1;
while (1) %contador_muestras <= numero_muestras
     pause(0.01);
     %clc
          
     %Gyrobaord Read
     [Data1, Data2, Data3] = getDataFromGyroboard(puerto_serial,32, SystemMode);

     
    fprintf(['Encoder 1: ' num2str(Data1)  '       Encoder 2: ' num2str(Data2) '     Encoder 3: ' num2str(Data3)   '  \n']); 
    t=t+1;
end

fclose(puerto_serial);