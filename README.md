# FFT GYRO files

This project contains several files and programs to use the FFT GYRO PRO version.

The multi-platform interface that we design is the FFTGyroTestTool, you can download and use it. You can download a copy of the source code and modify it to build your own version.

Here you can also fins some basic examples for to interface the FFT GYRO PRO with MATLAB and Simulink.

Here we will be umploading some user's manuals for reference.

[FFT GYRO Quick set up video.](https://youtu.be/n76H9Gta7Ok)
